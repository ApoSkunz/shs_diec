( function() {
    let consensus = document.getElementsByClassName('consensus');
    let arrow = document.getElementsByClassName('link-arrow');
    for (let i=0; i<consensus.length; i++) {
        consensus[i].style.display = 'none';
        arrow[i].style.display = 'none';
    }
}) ();

let mainConsensus = document.getElementsByClassName('consensus');
let arrow = document.getElementsByClassName('link-arrow');
let failureDot = document.getElementsByClassName('failure-dot');
let solutionDot = document.getElementsByClassName('solution-dot');

$(document).ready(function(){ 
    for (let i = 0; i < failureDot.length; i++) {
        $(failureDot[i]).click(function(){
            // Clean previous consensus
            for (let j = 0; j < failureDot.length; j++) {
                if (i!==j) {
                    solutionDot[j].removeAttribute('style');
                    failureDot[j].removeAttribute('style');
                    solutionDot[j].setAttribute('title',solutionDot[j].getAttribute('title').replace('fermer','voir'));
                    failureDot[j].setAttribute('title',solutionDot[j].getAttribute('title'));
                    mainConsensus[j].style.display = 'none';
                    arrow[j].style.display = 'none';
                }
            }
            // Update
            if (solutionDot[i].style.color !== 'rgb(8, 8, 8)') {
                mainConsensus[i].removeAttribute('style');
                arrow[i].removeAttribute('style');
                solutionDot[i].style.background = '#fdfdfd';
                failureDot[i].style.background = '#fdfdfd';
                failureDot[i].style.color = '#080808';
                solutionDot[i].style.color = '#080808';
                solutionDot[i].setAttribute('title',solutionDot[i].getAttribute('title').replace('voir','fermer'));
                failureDot[i].setAttribute('title',solutionDot[i].getAttribute('title'));
                arrow[i].setAttribute('data-opacity','TRUE');
                arrow[i].style.animation = "opacity-transition 1s linear 1";
                mainConsensus[i].style.animation = "opacity-transition 1s linear 1";
            } else {
                // Default value
                console.log('hhdhj');
                arrow[i].style.animation = "opacity-transition-reverse 1s linear 1";
                mainConsensus[i].style.animation = "opacity-transition-reverse 1s linear 1";
                setTimeout(function() {
                    if (arrow[i].getAttribute('data-opacity') === 'TRUE') {
                        arrow[i].removeAttribute('data-opacity');
                        solutionDot[i].removeAttribute('style');
                        failureDot[i].removeAttribute('style');
                        solutionDot[i].setAttribute('title',solutionDot[i].getAttribute('title').replace('fermer','voir'));
                        failureDot[i].setAttribute('title',solutionDot[i].getAttribute('title'));
                        mainConsensus[i].style.display = 'none';
                        arrow[i].style.display = 'none';
                    }
                }, 1000);
            }  
        });
        $(solutionDot[i]).click(function(){
            // Clean previous consensus
            for (let j = 0; j < failureDot.length; j++) {
                if (i!==j) {
                    solutionDot[j].removeAttribute('style');
                    failureDot[j].removeAttribute('style');
                    solutionDot[j].setAttribute('title',solutionDot[j].getAttribute('title').replace('fermer','voir'));
                    failureDot[j].setAttribute('title',solutionDot[j].getAttribute('title'));
                    mainConsensus[j].style.display = 'none';
                    arrow[j].style.display = 'none';
                }
            }
            // Update
            if (solutionDot[i].style.color !== 'rgb(8, 8, 8)') {
                mainConsensus[i].removeAttribute('style');
                arrow[i].removeAttribute('style');
                solutionDot[i].style.background = '#f2c56b';
                failureDot[i].style.background = '#f2c56b';
                failureDot[i].style.color = '#080808';
                solutionDot[i].style.color = '#080808';
                solutionDot[i].setAttribute('title',solutionDot[i].getAttribute('title').replace('voir','fermer'));
                failureDot[i].setAttribute('title',solutionDot[i].getAttribute('title'));
                arrow[i].setAttribute('data-opacity','TRUE');
                arrow[i].style.animation = "opacity-transition 1s linear 1";
                mainConsensus[i].style.animation = "opacity-transition 1s linear 1";
            } else {
                // Default value
                arrow[i].style.animation = "opacity-transition-reverse 1s linear 1";
                mainConsensus[i].style.animation = "opacity-transition-reverse 1s linear 1";
                setTimeout(function() {
                    if (arrow[i].getAttribute('data-opacity') === 'TRUE') {
                        arrow[i].removeAttribute('data-opacity');
                        solutionDot[i].removeAttribute('style');
                        failureDot[i].removeAttribute('style');
                        solutionDot[i].setAttribute('title',solutionDot[i].getAttribute('title').replace('fermer','voir'));
                        failureDot[i].setAttribute('title',solutionDot[i].getAttribute('title'));
                        mainConsensus[i].style.display = 'none';
                        arrow[i].style.display = 'none';
                    }
                }, 1000);
            }  
        });
        $(solutionDot[i]).hover(function(){
            solutionDot[i].style.background = '#f2c56b';
            failureDot[i].style.background = '#f2c56b';
        }, function(){
            if (solutionDot[i].style.color !== 'rgb(8, 8, 8)') {
                solutionDot[i].removeAttribute('style');
                failureDot[i].removeAttribute('style');
            } else {
                solutionDot[i].style.background = '#fdfdfd';
                failureDot[i].style.background = '#fdfdfd';
            }
        });
        $(failureDot[i]).hover(function(){
            solutionDot[i].style.background = '#f2c56b';
            failureDot[i].style.background = '#f2c56b';
        }, function(){
            if (solutionDot[i].style.color !== 'rgb(8, 8, 8)') {
                solutionDot[i].removeAttribute('style');
                failureDot[i].removeAttribute('style');
            } else {
                solutionDot[i].style.background = '#fdfdfd';
                failureDot[i].style.background = '#fdfdfd';
            }
        });
    }
});

function displayNoneTitles() {
    document.getElementById('title-h1').style.display = 'none';
    document.getElementById('title-h3').style.display = 'none';
}

function resetDisplay() {
    document.getElementById('active-link').removeAttribute('id');
    document.getElementById('failure-solution-container').removeAttribute('style');
    document.getElementById('question-container').removeAttribute('style');
    document.getElementById('link-container').removeAttribute('style');
    document.getElementById('conclusion-container').removeAttribute('style');
    document.getElementById('method-container').removeAttribute('style');
    displayNoneTitles();
}

function displayHome() {
    resetDisplay();
    document.getElementById('title-h1').removeAttribute('style');
    document.getElementById('title-h3').removeAttribute('style');
    document.getElementsByClassName('home-link')[0].setAttribute('id',"active-link");
}

function displayConsensus() {
    resetDisplay();
    document.getElementById('failure-solution-container').style.display = 'block';
    document.getElementsByClassName('main-link')[0].setAttribute('id',"active-link");
}

function displayInterro() {
    resetDisplay();
    document.getElementById('question-container').style.display = 'block';
    document.getElementsByClassName('main-link')[1].setAttribute('id',"active-link");
}

function displayLink() {
    resetDisplay();
    document.getElementById('link-container').style.display = 'block';
    document.getElementsByClassName('main-link')[2].setAttribute('id',"active-link");
}

function displayConclusion() {
    resetDisplay();
    document.getElementById('conclusion-container').style.display = 'block';
    document.getElementsByClassName('main-link')[3].setAttribute('id',"active-link");
}

function displayMethod() {
    resetDisplay();
    document.getElementById('method-container').style.display = 'block';
    document.getElementsByClassName('main-link')[4].setAttribute('id',"active-link");
}